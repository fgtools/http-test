/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <time.h>
#include <string>
#ifdef _MSC_VER
#include <WinSock2.h>
typedef int socklen_t;
#else
#include <stdlib.h> // for malloc(), ...
#include <string.h> // for strlen(), ...
#include <netinet/in.h> // for ntohl(), ...
#include <fcntl.h> // for fcntl(), ...
#include <sys/socket.h> // inet_addr ...
#include <netinet/in.h> // inet_addr ...
#include <arpa/inet.h> // inet_addr ...
#include <unistd.h> // for usleep(), ...
#endif
#include "sprtf.hxx"
#include "pollkbd.hxx"
#include "gen-utils.hxx"
#include "http-utils.hxx"
#include "file-server.hxx"

static const char *module = "http";

#ifndef SPRTF
#define SPRTF printf
#endif
static const char *in_file = 0;

#ifdef ADD_RECV_LOG
static const char *readlog = "tempread.log";
static FILE *rlog = 0;
#endif

static int even_max = 3;

static int port = DEF_SERVER_PORT;
static const char *serv_addr = DEF_SERVER_ADDRESS;
static int max_conns = DEF_MAX_CONNS;
static int ms_sleep = DEF_MS_SLEEP;
static int ms_timeout = DEF_MS_TIMEOUT;
static char *user_log = 0;

int netInit()
{
#ifdef _MSC_VER
    // WSAStartup()
    WORD wVersionRequested;
    WSADATA wsaData;
    int err;
    /* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
    wVersionRequested = MAKEWORD(2, 2);
    err = WSAStartup(wVersionRequested, &wsaData);
    if (err != 0) {
        /* Tell the user that we could not find a usable */
        /* Winsock DLL.                                  */
        //SPRTF("%s: WSAStartup failed with error: %d\n", module, err);
        PERROR("WSAStartup failed");
        return 1;
    }
#endif // _MSC_VER
    return 0;
}

int netClose()
{
#ifdef _MSC_VER
	/* Clean up windows networking */
    int err = WSACleanup();
	if ( err == SOCKET_ERROR ) {
		if ( WSAGetLastError() == WSAEINPROGRESS ) {
			WSACancelBlockingCall();
			WSACleanup();
		}
	}
#endif // _MSC_VER
    return 0;
}

const char *getClientIP(struct sockaddr_in *addr)
{
    static char buf [32];
	long x = ntohl(addr->sin_addr.s_addr);
	sprintf(buf, "%d.%d.%d.%d",
		(int) (x>>24) & 0xff, (int) (x>>16) & 0xff,
		(int) (x>> 8) & 0xff, (int) (x>> 0) & 0xff );
    return buf;
}

unsigned int getClientPort(struct sockaddr_in *addr)
{
    return ntohs(addr->sin_port);
}

void setNonBlocking ( int fd )
{
#if defined(WIN32)
    u_long nblocking = 1;
    ioctlsocket(fd, FIONBIO, &nblocking);
#else
    int delay_flag = ::fcntl(fd, F_GETFL, 0);
    delay_flag &= (~O_NDELAY);
    fcntl(fd, F_SETFL, delay_flag);
#endif
}



int check_and_delay()
{
    int res = test_for_input();
    if (res) {
        if (res == 0x1b) {
            SPRTF("%s: Got ESC exit key...\n", module );
            return 1;
        } else {
            SPRTF("%s: Got unknown key %X!\n", module, res );
        }
    }
    if (ms_sleep > 0) {
        SLEEP(ms_sleep);
    }
    return 0;
}

static char header[1024];
size_t set_http_header(int contLen)
{
    // "Server: Apache/2.2.3" EOL
    // "Last-Modified: Wed, 18 Jun 2003 16:05:58 GMT" EOL
    // "ETag: \"56d-9989200-1132c580\"" EOL
    strcpy(header, "HTTP/1.1 200 OK" EOL);
    sprintf(EndBuf(header), "Date: %s UTC" EOL, Get_Current_UTC_Time_Stg());
    sprintf(EndBuf(header), "Content-Type: %s" EOL, get_content_type());
    sprintf(EndBuf(header), "Content-Length: %d" EOL, contLen);
    if (is_content_binary()) {
        strcat(header,"Accept-Ranges: bytes" EOL);
    }
    sprintf(EndBuf(header),"Content-Disposition: inline; filename=%s" EOL, get_file_name((char *)in_file) ) ;

    strcat(header, "Connection: close" EOL);
    // from : https://drupal.org/node/417866
    // <FilesMatch "\.(?i:doc|odf|pdf|rtf|txt)$">
    // Header set Content-Disposition attachment
    // ForceType application/octet-stream
    // from : http://stackoverflow.com/questions/4679756/show-a-pdf-files-in-users-browser-via-php-perl
    /*  header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $filename . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');  */

    SPRTF("%s: HTTP header len %d, set to\n%s\n", module, (int)(strlen(header)+2), header);
    // ================================
    // add final second EOL
    strcat(header,EOL);
    // ================================
    return strlen(header);
}

#define MMAX_SOCKS 10

int sendFile( char *name )
{
    int iret = 0;
    FILE *file;
    char *buffer;
    int fileLen, res;
    int size, client;

    if (is_file_or_directory(name) != 1) {
        SPRTF("%s: failed to 'stat' file %s\n", module, name );
        return 1;
    }
    //Get file length
    fileLen = (int)get_last_file_size();
    //fseek(file, 0, SEEK_END);
    //fileLen = ftell(file);
    //fseek(file, 0, SEEK_SET);
    if (fileLen == 0) {
        //fclose(file);
        SPRTF("%s: File file [%s] is null lenght.\n", module, name );
        return 2;
    }

    //Open file
    file = fopen(name, "rb");
    if (!file) {
        SPRTF("%s: FAILED to 'open' file [%s]\n", module, name );
        return 1;
    }

    //Allocate memory
    buffer = (char *)malloc(fileLen+1);
    if (!buffer) {
        fprintf(stderr, "%s: Memory error!", module);
        fclose(file);
        return 3;
    }

    // Read file contents into buffer
    res = fread(buffer, 1, fileLen, file);
    fclose(file);
    if (res != fileLen) {
        SPRTF("%s: file read failed! req %d, got %d\n", module, fileLen, res);
        return 4;
    }

    // ok time to setup the reply header
    int headLen = (int)set_http_header(fileLen);
    int total = headLen+fileLen;
    char *reply = (char*)malloc(total);
    if (!reply) {
        SPRTF("%s: buffer memory failed on %d bytes!\n", module,total);
        return 5;
    }

    strcpy(reply, header);
    memcpy(reply+headLen, buffer, fileLen);

    SPRTF("%s: got reply of %d bytes, from file %s\n", module, total, name);

    //int sd = socket(PF_INET, SOCK_STREAM, 0);
    int sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sd <= 0 ) {
        PERROR("create socket failed!\n");
        free(reply);
        return 6;
    }

    struct sockaddr_in in_addr;
    memset(&in_addr, 0, sizeof(in_addr));
    //in_addr.sin_family = AF_INET;
    //in_addr.sin_port = htons(port);
    //in_addr.sin_addr.s_addr = INADDR_ANY;

    in_addr.sin_family = AF_INET;
    in_addr.sin_port = htons(port);
    in_addr.sin_addr.s_addr = inet_addr(serv_addr);

    res = bind(sd, (sockaddr *)&in_addr, sizeof(in_addr) );
    if( res != 0 ) {
        PERROR("bind error");
        free(reply);
        SCLOSE(sd);
        return 7;
    }

    MSocket *preads[MMAX_SOCKS];
    MSocket *pwrites[MMAX_SOCKS];
    MSocket rs,ws,cs;
    int i;
    char *rb;
    rs.setHandle(sd);
    setNonBlocking(sd);
    res = listen(sd, max_conns); 
    if ( res != 0 ) {
        PERROR("listen error");
        free(reply);
        SCLOSE(sd);
        return 8;
    }
    int off = 0;
    bool ok = true;
    SPRTF("%s: Waiting on %s, port %d for HTTP GET request...\n", module, serv_addr, port );
    while (ok) {
        // only waiting for read request
        preads[0] = &rs;
        preads[1] = 0;
        res = Socketselect ( preads, 0, ms_timeout );
        if (res == -1) {
            PERROR("select error");
            iret = 10;
            break;
        } else if (res == -2) {
            res = check_and_delay();
            if (res)
                break;
            continue;   // wait some more
        }

        off = 0; // restart offset, and
        total = headLen+fileLen; // total to send
        size = sizeof(in_addr);
        // accept the client
        client = accept(sd, (sockaddr *)&in_addr, (socklen_t *)&size);
        if (SERROR(client)) {
            PERROR("accept failed");
            iret = 11;
            ok = false;
        } else {
            SPRTF("%s: accepted client %d addr %s, port %u\n", module, 
                client, getClientIP(&in_addr), getClientPort(&in_addr));
            // inner service this client
            ///////////////////////////////////////////////////////////////////
            // cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
            int events = 0;
            bool noerr = true;
            while (noerr) {
                int rdcnt = 0;
                int wrcnt = 0;
                cs.setHandle(client);
                preads[0] = &cs;
                preads[1] = 0;
                pwrites[0] = 0;
                if (total) {
                    // have data to send
                    ws.setHandle(client);
                    pwrites[0] = &ws;
                    pwrites[1] = 0;
                    res = Socketselect ( preads, pwrites, ms_timeout );
                } else {
                    // check for more reads...
                    res = Socketselect ( preads, 0, ms_timeout );
                }
                if (res == -1) {
                    PERROR("select rw error");
                    break;
                } else if (res == -2) {
                    res = check_and_delay();
                    if (res) {
                        ok = false;
                        break;
                    }
                    continue;
                }
                // check the reads
                // rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
                for (i = 0; i < MMAX_SOCKS; i++) {
                    if (preads[i]) {
                        rb = GetNxtBuf();
                        res = recv(preads[i]->getHandle(),rb,512,0);
                        if (SERROR(res)) {
                            PERROR("recv error");
                            noerr = false; // out of while
                            break;  // out of for
                        } else if (res) {
                            SPRTF("%s: read %d bytes.\n", module, res );
#ifdef ADD_RECV_LOG
                            if (rlog != (FILE *)-1) {
                                rlog = fopen(readlog,"a");
                                if (rlog) {
                                    int res2;
                                    char *tmp = GetNxtBuf();
                                    res2 = sprintf(tmp,"======= client %d, addr %s, port %d ========\n",
                                        client, getClientIP(&in_addr), getClientPort(&in_addr)); 
                                    fwrite(tmp,1,res2,rlog);
                                    res2 = fwrite(rb,1,res,rlog);
                                    if (res2 != res) {
                                        fclose(rlog);
                                        SPRTF("%s: Read log write FAILED\n", module);
                                        rlog = (FILE *)-1;
                                    }
                                    fclose(rlog);
                                }
                            }
#endif // ADD_RECV_LOG
                            rdcnt++;
                        }
                    } else
                        break;
                }
                // rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr
                // check the writes
                // wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
                for (i = 0; i < MMAX_SOCKS; i++) {
                    if (pwrites[i]) {
                        if (total) {
                            res = send(client, reply+off, total, 0);
                            if (SERROR(res)) {
                                PERROR("send error");
                                noerr = false; // out of while
                                break;  // out of for
                            } else {
                                SPRTF("%s: sent %d bytes\n", module, res );
                                if ((res > 0) && (res < total)) {
                                    total -= res;
                                    off += res;
                                } else {
                                    total = 0;
                                }
                            }
                            wrcnt++;
                        }
                    } else
                        break;
                }
                // wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
                events++;
                if ((rdcnt == 0) && (wrcnt == 0) && (total == 0) && (events > even_max)) {
                    // no reads, writes and no more to send
                    SCLOSE(client);
                    SPRTF("%s: closed client %d\n", module, client);
                    SPRTF("%s: Waiting on %s, port %d for HTTP GET request...\n", module, serv_addr, port );
                    break;  // back to wait for NEXT client
                }
                res = check_and_delay();
                if (res) {
                    ok = false;
                    break;
                }
            }
            ///////////////////////////////////////////////////////////////////
            // cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
        }
    }
    SCLOSE(sd);
    free(reply);
    return iret;
}

void give_help(char *name) 
{
    char *fn = get_file_name(name);
    SPRTF("\n");
    SPRTF("%s - version " HTTP_VERSION "\n", fn);
    SPRTF("\n");
    SPRTF("Usage: %s [options] in_file\n", fn);
    SPRTF("\n");
    SPRTF("Options:\n");
    SPRTF(" --help   (-h or -?) = This help and exit(0)\n");
    SPRTF(" --port nnnn    (-p) = Set port (def=%d)\n", port);
    //SPRTF(" --json <file>  (-j) = Set the json file to use. (def=%s)\n", get_file_name((char *)json_file));
    SPRTF(" --addr <ip>    (-a) = Set the IP address. (def=%s)\n", serv_addr );
    SPRTF(" --sleep <ms>   (-s) = Set milliseconds sleep after sned. 0 for none. (def=%d)\n", ms_sleep);
    SPRTF(" --timeout <ms> (-t) = Set milliseconds select timeout. (def=%d)\n", ms_timeout);
    SPRTF(" --log <file>   (-l) = Set log file.\n    (def=%s)\n", get_log_file());
    //dem_path_help();
    SPRTF("\n");
    SPRTF("Listen on address:port, and on connection send in_file using\n");
    SPRTF("the file extension to set the Content-type.\n");
    SPRTF("Allowed extensions ");
    show_extensions();
    SPRTF("\n");
    SPRTF("\n");
    //SPRTF("If successful, exit(0), else exit with an error value\n");
    SPRTF("Use an ESC keyin to exit the continuous server loop.\n");
    SPRTF("\n");
}

int parse_args( int argc, char **argv )
{
    int i, i2, c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        i2 = i + 1;
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'l':
                if (i2 < argc) {
                    i++;
                    user_log = strdup(argv[i]);
                    SPRTF("%s: Set listen port to %d\n", module, port );
                } else {
                    SPRTF("%s: expected log file name to follow [%s]\n", module, arg );
                    return 1;
                }
                break;
            case 'p':
                if (i2 < argc) {
                    i++;
                    port = atoi(argv[i]);
                    SPRTF("%s: Set listen port to %d\n", module, port );
                } else {
                    SPRTF("%s: expected port number to follow [%s]\n", module, arg );
                    return 1;
                }
                break;
            case 's':
                if (i2 < argc) {
                    i++;
                    ms_sleep = atoi(argv[i]);
                    SPRTF("%s: Set ms sleep to %d\n", module, ms_sleep );
                } else {
                    SPRTF("%s: expected ms sleep value to follow [%s]\n", module, arg );
                    return 1;
                }
                break;
            case 't':
                if (i2 < argc) {
                    i++;
                    ms_timeout = atoi(argv[i]);
                    SPRTF("%s: Set ms select timeout to %d\n", module, ms_timeout );
                } else {
                    SPRTF("%s: expected ms timeout value to follow [%s]\n", module, arg );
                    return 1;
                }
                break;
            default:
                SPRTF("%s: Unknown argument [%s]\n", module, arg );
                return 1;
                break;
            }
        } else {
            // assume a file to send
            if (is_file_or_directory(arg) == 1) {
                if (set_extension_type(arg)) {
                    in_file = strdup(arg);
                    SPRTF("%s: Set file to send as [%s], %d bytes, content-type %s\n", module, 
                        in_file, (int)get_last_file_size(), get_content_type());
                } else {
                    SPRTF("%s: Unable to determined content-type from extension of file [%s]!\n", module, arg);
                    SPRTF("Allowed extensions ");
                    show_extensions();
                    SPRTF("\n");
                    return 1;

                }
            } else {
                SPRTF("%s: Unable to 'stat' file [%s]!\n", module, arg);
                return 1;
            }
        }
    }
    if (in_file == 0) {
        SPRTF("%s: no in file found in command!\n", module);
        return 1;
    }
    return 0;
}

int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)"none",false);
    if (argc < 2) {
        give_help(argv[0]);
        SPRTF("%s: Error: Need at least a in file name!\n", module);
        return 1;
    }
    iret = parse_args(argc,argv);
    if (iret) {
        if (iret == 2)
            return 0;
        else
            return iret;
    }
    // setup the LOG file
    if (user_log)
        set_log_file(user_log);
    else
        set_log_file(get_log_file());

    if (netInit() != 0) {
        SPRTF("%s: netInit FAILED!\n", module );
        return 1;
    }

    add_sys_time(1);

    iret = sendFile( (char *)in_file );

    netClose();

    return iret;

}

// eof
