/*\
 * win-protos.cxx
 *
 * Copyright (c) 2015 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#include <stdio.h>
#include <string.h> // for strdup(), ...
#include <WinSock2.h>   // includes <windows.h>
#include "sprtf.hxx"

#ifndef SPRTF
#define SPRTF printf
#endif

static const char *module = "win-protos";
static const char *def_log = "tempproto.txt";

void give_help( char *name )
{
    char *cn = getenv("COMPUTERNAME");
    SPRTF("\n");
    SPRTF("%s: usage: [options]\n", module);
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = This help and exit(2)\n");
    SPRTF("\n");
    SPRTF(" Sole purpose is to enumerate protocols installed in this\n");
    SPRTF(" Windows %s computer.\n", cn ? cn : "");
    SPRTF("\n");
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            default:
                SPRTF("%s: Unknown argument '%s'. Try -? for help...\n", module, arg);
                return 1;
            }
        } else {
            SPRTF("%s: Unknown argument '%s'. Try -? for help...\n", module, arg);
            return 1;
        }
    }
    return 0;
}

static LPSTR getError(int ErrorCode)
{
    static char Message[1024];

    // If this program was multithreaded, we'd want to use
    // FORMAT_MESSAGE_ALLOCATE_BUFFER instead of a static buffer here.
    // (And of course, free the buffer when we were done with it)

    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS |
                  FORMAT_MESSAGE_MAX_WIDTH_MASK, NULL, ErrorCode,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                  (LPSTR) Message, 1024, NULL);
    return Message;
}


static int win_net_init()
{
    WSADATA wsaData;
    int RetVal;

    // Ask for Winsock version 2.2.
    RetVal = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (RetVal != 0) {
        RetVal = WSAGetLastError();
        fprintf(stderr, "WSAStartup failed with error %d: %s\n",
                RetVal, getError(RetVal));
        WSACleanup();
        return 1;
    }
    return 0;
}

static void win_net_close()
{
    WSACleanup();
}

/*
typedef struct _WSAPROTOCOL_INFO {
  DWORD            dwServiceFlags1;
  DWORD            dwServiceFlags2;
  DWORD            dwServiceFlags3;
  DWORD            dwServiceFlags4;
  DWORD            dwProviderFlags;
  GUID             ProviderId; // GUID (or UUID) is an acronym for 'Globally Unique Identifier' (or 'Universally Unique Identifier'). It is a 128-bit integer number used to identify resources.
  DWORD            dwCatalogEntryId; // A unique identifier assigned by the WS2_32.DLL for each WSAPROTOCOL_INFO structure.
  WSAPROTOCOLCHAIN ProtocolChain; // see structure below
  int              iVersion;    // The protocol version identifier.
  int              iAddressFamily; // show_PFamily()
  int              iMaxSockAddr;    // The maximum address length, in bytes.
  int              iMinSockAddr;    // The minimum address length, in bytes.
  int              iSocketType;
  int              iProtocol;
  int              iProtocolMaxOffset;
  int              iNetworkByteOrder;
  int              iSecurityScheme;
  DWORD            dwMessageSize;
  DWORD            dwProviderReserved;
  TCHAR            szProtocol[WSAPROTOCOL_LEN+1];
} WSAPROTOCOL_INFO, *LPWSAPROTOCOL_INFO;

A GUID is most commonly written in text as a sequence of hexadecimal digits separated into five groups, such as:
3F2504E0-4F89-41D3-9A0C-0305E82C3301
8	Data1
4	Data2
4	Data3
4	Initial two bytes from Data4
12	Remaining six bytes from Data4

typedef struct _WSAPROTOCOLCHAIN {
  int   ChainLen;
  DWORD ChainEntries[MAX_PROTOCOL_CHAIN];
} WSAPROTOCOLCHAIN, *LPWSAPROTOCOLCHAIN;

*/

typedef struct tagDWFLAGS {
    DWORD bit;
    const char *desc;
}DWFLAGS, *PDWFLAGS;

static DWFLAGS ServiceFlags1[] = {
    { XP1_CONNECTIONLESS, "Provides connectionless (datagram) service. If not set, the protocol supports connection-oriented data transfer." },
    { XP1_GUARANTEED_DELIVERY, "Guarantees that all data sent will reach the intended destination." },
    { XP1_GUARANTEED_ORDER, "Guarantees that data only arrives in the order in which it was sent and that it is not duplicated. This characteristic does not necessarily mean that the data is always delivered, but that any data that is delivered is delivered in the order in which it was sent." },
    { XP1_MESSAGE_ORIENTED, "Honors message boundaries�as opposed to a stream-oriented protocol where there is no concept of message boundaries." },
    { XP1_PSEUDO_STREAM, "A message-oriented protocol, but message boundaries are ignored for all receipts. This is convenient when an application does not desire message framing to be done by the protocol." },
    { XP1_GRACEFUL_CLOSE, "Supports two-phase (graceful) close. If not set, only abortive closes are performed." },
    { XP1_EXPEDITED_DATA, "Supports expedited (urgent) data." },
    { XP1_CONNECT_DATA, "Supports connect data." },
    { XP1_DISCONNECT_DATA, "Supports disconnect data." },
    { XP1_SUPPORT_BROADCAST, "Supports a broadcast mechanism." },
    { XP1_SUPPORT_MULTIPOINT, "Supports a multipoint or multicast mechanism. Control and data plane attributes are indicated below." },
    { XP1_MULTIPOINT_CONTROL_PLANE, "Indicates whether the control plane is rooted (value = 1) or nonrooted (value = 0)." },
    { XP1_MULTIPOINT_DATA_PLANE, "Indicates whether the data plane is rooted (value = 1) or nonrooted (value = 0)." },
    { XP1_QOS_SUPPORTED, "Supports quality of service requests." },
    { XP1_INTERRUPT, "Bit is reserved." },
    { XP1_UNI_SEND, "Protocol is unidirectional in the send direction." },
    { XP1_UNI_RECV, "Protocol is unidirectional in the recv direction." },
    { XP1_IFS_HANDLES, "Socket descriptors returned by the provider are operating system Installable File System (IFS) handles." },
    { XP1_PARTIAL_MESSAGE, "The MSG_PARTIAL flag is supported in WSASend and WSASendTo." },
    { XP1_SAN_SUPPORT_SDP, "The protocol provides support for SAN. This value is supported on Windows 7 and Windows Server 2008 R2." },
    // END
    { 0, 0 }
};

void show_ServiceFlags1( DWORD flag )
{
    PDWFLAGS pf = ServiceFlags1;
    SPRTF("Show of ServiceFlags1 flags %#X\n", flag);
    while (pf->desc) {
        if (pf->bit & flag) {
            SPRTF(" %s\n", pf->desc);
            flag &= ~pf->bit;
            if (!flag)
                break;
        }
        pf++;
    }
}

static DWFLAGS ProviderFlags[] = {
    { PFL_MULTIPLE_PROTO_ENTRIES, "0x00000001 Indicates that this is one of two or more entries for a single protocol (from a given provider) which is capable of implementing multiple behaviors. An example of this is SPX which, on the receiving side, can behave either as a message-oriented or a stream-oriented protocol." },
    { PFL_RECOMMENDED_PROTO_ENTRY, "0x00000002 Indicates that this is the recommended or most frequently used entry for a protocol that is capable of implementing multiple behaviors." },
    { PFL_HIDDEN, "0x00000004 Set by a provider to indicate to the Ws2_32.dll that this protocol should not be returned in the result buffer generated by WSAEnumProtocols. Obviously, a Windows Sockets 2 application should never see an entry with this bit set." },
    { PFL_MATCHES_PROTOCOL_ZERO, "0x00000008 Indicates that a value of zero in the protocol parameter of socket or WSASocket matches this protocol entry." },
    { PFL_NETWORKDIRECT_PROVIDER, "0x00000010 Set by a provider to indicate support for network direct access. This value is supported on Windows 7 and Windows Server 2008 R2." },
    // END
    { 0, 0 }

};

void show_ProviderFlags( DWORD flag )
{
    PDWFLAGS pf = ProviderFlags;
    SPRTF("Show of ProviderFlags flags %#X\n", flag);
    while (pf->desc) {
        if (pf->bit & flag) {
            SPRTF(" %s\n", pf->desc);
            flag &= ~pf->bit;
            if (!flag)
                break;
        }
        pf++;
    }
}


static DWFLAGS PFamily[] = {
    { AF_INET, "2 The Internet Protocol version 4 (IPv4) address family." },
    { AF_IPX, "6 The IPX/SPX address family. This address family is only supported if the NWLink IPX/SPX NetBIOS Compatible Transport protocol is installed. This address family is not supported on Windows Vista and later." },
    { AF_APPLETALK, "16 The AppleTalk address family. This address family is only supported if the AppleTalk protocol is installed. This address family is not supported on Windows Vista and later." },
    { AF_NETBIOS, "17 The NetBIOS address family. This address family is only supported if the Windows Sockets provider for NetBIOS is installed." },
    // The Windows Sockets provider for NetBIOS is supported on 32-bit versions of Windows. This provider is installed by default on 32-bit versions of Windows.
    // The Windows Sockets provider for NetBIOS is not supported on 64-bit versions of windows including Windows 7, Windows Server 2008, Windows Vista, Windows Server 2003, or Windows XP.
    // The Windows Sockets provider for NetBIOS only supports sockets where the type parameter is set to SOCK_DGRAM.
    // The Windows Sockets provider for NetBIOS is not directly related to the NetBIOS programming interface. The NetBIOS programming interface is not supported on Windows Vista, Windows Server 2008, and later.
    { AF_INET6, "23 The Internet Protocol version 6 (IPv6) address family." },
    { AF_IRDA, "26 The Infrared Data Association (IrDA) address family. This address family is only supported if the computer has an infrared port and driver installed." },
    { AF_BTH, "32 The Bluetooth address family. This address family is supported on Windows XP with SP2 or later if the computer has a Bluetooth adapter and driver installed." },
    // END
    { 0, 0 }
};

void show_PFamily( DWORD flag )
{
    PDWFLAGS pf = PFamily;
    //SPRTF("Show of PFamily %#X\n", flag);
    while (pf->desc) {
        if (pf->bit == flag) {
            SPRTF("Protcol Family: %s\n", pf->desc);
            return;
        }
        pf++;
    }
    SPRTF("Protocol Family %u not in list\n", flag);
}

static DWFLAGS SocketType[] = {
    { SOCK_STREAM, "1 A socket type that provides sequenced, reliable, two-way, connection-based byte streams with an OOB data transmission mechanism. This socket type uses the Transmission Control Protocol (TCP) for the Internet address family (AF_INET or AF_INET6)." },
    { SOCK_DGRAM, "2 A socket type that supports datagrams, which are connectionless, unreliable buffers of a fixed (typically small) maximum length. This socket type uses the User Datagram Protocol (UDP) for the Internet address family (AF_INET or AF_INET6)." },
    { SOCK_RAW, "3 A socket type that provides a raw socket that allows an application to manipulate the next upper-layer protocol header. To manipulate the IPv4 header, the IP_HDRINCL socket option must be set on the socket. To manipulate the IPv6 header, the IPV6_HDRINCL socket option must be set on the socket." },
    { SOCK_RDM, "4 A socket type that provides a reliable message datagram. An example of this type is the Pragmatic General Multicast (PGM) multicast protocol implementation in Windows, often referred to as reliable multicast programming." },
    // This value is only supported if the Reliable Multicast Protocol is installed.
    { SOCK_SEQPACKET, "5 A socket type that provides a pseudo-stream packet based on datagrams." },
    // end
    { 0, 0 }
};

void show_SocketType( DWORD flag )
{
    PDWFLAGS pf = SocketType;
    //SPRTF("Show of SocketType %#X\n", flag);
    while (pf->desc) {
        if (pf->bit == flag) {
            SPRTF("Socket Type:\t %s\n", pf->desc);
            return;
        }
        pf++;
    }
    SPRTF("SocketType %u not in list\n", flag);
}

#ifndef BTHPROTO_RFCOMM
#define BTHPROTO_RFCOMM 3
#endif
#ifndef IPPROTO_RM
#define IPPROTO_RM 113
#endif

static DWFLAGS Protocol[] = {
    { IPPROTO_ICMP, "1 The Internet Control Message Protocol (ICMP)." },
    // This value is supported on Windows XP and later.
    { IPPROTO_IGMP, "2 The Internet Group Management Protocol (IGMP)." },
    // This value is supported on Windows XP and later.
    { BTHPROTO_RFCOMM, "3 The Bluetooth Radio Frequency Communications (Bluetooth RFCOMM) protocol." },
    // This value is supported on Windows XP with SP2 or later.
    { IPPROTO_TCP, "6 The Transmission Control Protocol (TCP)." },
    { IPPROTO_UDP, "17 The User Datagram Protocol (UDP)." },
    { IPPROTO_ICMPV6, "58 The Internet Control Message Protocol Version 6 (ICMPv6)." },
    // This value is supported on Windows XP and later.
    { IPPROTO_RM, "113 The PGM protocol for reliable multicast. On the Windows SDK released for Windows Vista and later, this protocol is also called IPPROTO_PGM." },
    // This value is only supported if the Reliable Multicast Protocol is installed.
    // end
    { 0, 0 }
};

void show_Protocol( DWORD flag )
{
    PDWFLAGS pf = Protocol;
    //SPRTF("Show of Protocol %#X\n", flag);
    while (pf->desc) {
        if (pf->bit == flag) {
            SPRTF("Protocol:\t %s\n", pf->desc);
            return;
        }
        pf++;
    }
    SPRTF("Protocol %u not in list\n", flag);
}

int enum_protocols()
{
    DWORD buflen = 0;
    LPWSAPROTOCOL_INFO pi = 0;
    LPWSAPROTOCOL_INFO p;
    char GuidString[128] = { 0 };

    int i, ret, iret = win_net_init();
    if (iret)
        return iret;
    iret = WSAEnumProtocols(0,pi,&buflen);
    if (iret != SOCKET_ERROR) {
        iret = WSAGetLastError();
        fprintf(stderr, "WSAEnumProtocols failed with error %d: %s\n",
                iret, getError(iret));
        iret = 1;
        goto exit;
    }
    pi = (LPWSAPROTOCOL_INFO) malloc(buflen);
    if (!pi) {
        fprintf(stderr, "Memory allocation of %d bytes FAILED!\n", buflen);
        iret = 1;
        goto exit;
    }
    iret = WSAEnumProtocols(0,pi,&buflen);
    if (iret == SOCKET_ERROR) {
        iret = WSAGetLastError();
        fprintf(stderr, "WSAEnumProtocols failed with error %d: %s\n",
                iret, getError(iret));
        iret = 1;
        goto exit;
    }
    SPRTF("Got %d enumerated protocols...\n\n",iret);
    p = pi;
    i = 0;
    while (iret) {
        i++;
        SPRTF("Winsock Catalog Provider Entry #%d\n", i);
        SPRTF("Entry type:\t ");
        if (p->ProtocolChain.ChainLen == 1)
            SPRTF("Base Service Provider\n");
        else
            SPRTF("Layered Chain Entry\n");
        // NOTE: This is a UNICODE string
        ret = StringFromGUID2(p->ProviderId,
                            (LPOLESTR) & GuidString, 40);
        if (ret == 0)
            SPRTF("StringFromGUID2 failed\n");
        else
            SPRTF("Provider ID:\t %ws\n", GuidString);

        SPRTF("Catalog Entry ID:\t\t %u\n", p->dwCatalogEntryId);

        SPRTF("Version:\t\t\t %d\n", p->iVersion);
        if (p->iAddressFamily)
            show_PFamily(p->iAddressFamily);
        else
            SPRTF("Address Family:\t %d\n", p->iAddressFamily);
        SPRTF("Max Socket Address Length:\t %d\n", p->iMaxSockAddr);
        SPRTF("Min Socket Address Length:\t %d\n", p->iMinSockAddr);
        if (p->iSocketType)
            show_SocketType(p->iSocketType);
        else
            SPRTF("Socket Type:\t\t\t %d\n", p->iSocketType);
        if (p->iProtocol)
            show_Protocol(p->iProtocol);
        else
            SPRTF("Socket Protocol:\t %d\n", p->iProtocol);
        SPRTF("Socket Protocol Max Offset:\t %d\n", p->iProtocolMaxOffset);
        SPRTF("Network Byte Order:\t\t %d\n", p->iNetworkByteOrder);
        SPRTF("Security Scheme:\t\t %d\n", p->iSecurityScheme);
        SPRTF("Max Message Size:\t\t %u\n", p->dwMessageSize);
        if (p->dwServiceFlags1)
            show_ServiceFlags1(p->dwServiceFlags1);
        else
            SPRTF("ServiceFlags1:\t 0x%x\n", p->dwServiceFlags1);
        if (p->dwServiceFlags2)
            SPRTF("ServiceFlags2:\t 0x%x\n", p->dwServiceFlags2);
        if (p->dwServiceFlags3)
            SPRTF("ServiceFlags3:\t 0x%x\n", p->dwServiceFlags3);
        if (p->dwServiceFlags4)
            SPRTF("ServiceFlags4:\t 0x%x\n", p->dwServiceFlags4);

        if (p->dwProviderFlags)
            show_ProviderFlags(p->dwProviderFlags);
        else
            SPRTF("ProviderFlags:\t 0x%x\n", p->dwProviderFlags);

        SPRTF("Protocol Chain length:\t %d\n", p->ProtocolChain.ChainLen);

        SPRTF("\n");
        p++;
        iret--;
    }
    SPRTF("%s: Results written to '%s'\n", module, def_log);
exit:
    if (pi)
        free(pi);
    win_net_close();
    return iret;
}

// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log,0);
    iret = parse_args(argc,argv);
    if (iret)
        return iret;

    iret = enum_protocols();    // actions of app

    return iret;
}


// eof = win-protos.cxx
