/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/
#ifndef _FILE_SERVER_HXX_
#define _FILE_SERVER_HXX_

#ifdef _MSC_VER
extern void win_wsa_perror( char *msg );
    #define SWRITE(a,b,c) send(a,b,c,0)
    #define SREAD(a,b,c)  recv(a,b,c,0)
    #define SCLOSE closesocket
    #define SERROR(a) (a == SOCKET_ERROR)
    #define PERROR(a) win_wsa_perror(a)
#else
    #define SWRITE write
    #define SREAD  read
    #define SCLOSE close
    #define SERROR(a) (a < 0)
    #define PERROR(a) perror(a)
#endif


#define EOL "\r\n"

#ifndef DEF_SERVER_PORT
#define DEF_SERVER_PORT 5555
#endif

#ifndef DEF_SERVER_ADDRESS
#define DEF_SERVER_ADDRESS		"127.0.0.1"
#endif

#ifndef DEF_MAX_CONNS
#ifdef _MSC_VER
#define DEF_MAX_CONNS SOMAXCONN
#else
#define DEF_MAX_CONNS 16
#endif
#endif

#ifndef DEF_MS_SLEEP
#define DEF_MS_SLEEP 100
#endif

#ifndef SLEEP
#ifdef _MSC_VER
#define SLEEP(x) Sleep(x)
#else // !_MSC_VER
#define SLEEP(x) usleep( x * 1000 )
#endif // _MSC_VER y/n
#endif // SLEEP
#ifndef DEF_MS_TIMEOUT
#define DEF_MS_TIMEOUT 500
#endif

#endif // #ifndef _FILE_SERVER_HXX_
// eof
