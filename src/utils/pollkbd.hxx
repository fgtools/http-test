/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/
#ifndef _POLLKBD_HXX_
#define _POLLKBD_HXX_

extern int test_for_input();    // returns charater or 0


#endif // #ifndef _POLLKBD_HXX_
// eof - pollkbd.hxx

