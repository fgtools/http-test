/*\
 * mime-type.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _MIME_TYPE_HXX_
#define _MIME_TYPE_HXX_
#include <string>

extern const char *get_mime_type_from_ext( std::string ext );

#endif // #ifndef _MIME_TYPE_HXX_
// eof - mime-type.hxx
