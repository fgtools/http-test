/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/
#ifndef _GEN_UTILS_HXX_
#define _GEN_UTILS_HXX_
#include <string>

#ifdef _MSC_VER
// get a message from the system for this error value
extern char *get_errmsg_text( int err );
extern void win_wsa_perror( char *msg );
#endif

extern int is_file_or_directory( char *file );
extern size_t get_last_file_size(); // { return stat_buf.st_size; }

extern char *Get_Current_UTC_Time_Stg();
extern char *Get_UTC_Time_Stg(time_t Timestamp);

extern char *get_file_name(char *file);

// added 20140918
extern int InStr( char *lpb, char *lps );
extern int InStri( char *lpb, char *lps );
extern void ensure_win_seps( std::string &path );
extern void ensure_unix_seps( std::string &path );

#endif // #ifndef _GEN_UTILS_HXX_
// eof

