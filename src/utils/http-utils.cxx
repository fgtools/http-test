/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/

#include <stdio.h>
#include <string>
#ifdef _MSC_VER
#include <WinSock2.h>
#else
    // TODO for unix
#include <string.h> // for strlen(), ...
#include <sys/select.h> // for fd_set(), ...
#include <errno.h> // for errno, ...
#include <netinet/in.h> // for ntohl(), ...
#define stricmp strcasecmp
#endif
#include "sprtf.hxx"
#include "gen-utils.hxx"
#include "http-utils.hxx"

static const char *module = "http-utils";

#ifdef _MSC_VER
#pragma comment (lib, "Ws2_32.lib")
//#pragma comment (lib, "Mswsock.lib")
//#pragma comment (lib, "AdvApi32.lib")
//#pragma comment (lib, "Winmm.lib") // __imp__timeGetTime@0
#endif

////////////////////////////////////////////////////////////////////////////////
typedef struct tagCONTTYPES {
    const char *name;
    bool is_binary;
}CONTTYPES, *PCONTTYPES;

static CONTTYPES cont_types[ct_max] = {
    { "application/json" , false },
    { "text/csv" ,         false },
    { "text/html" ,        false },
    { "text/xml" ,         false },
    { "text/plain" ,       false },
    { "image/png" ,        true  },
    { "image/bmp",         true  }
};

static const char *content_types[ct_max] = {
     "application/json" ,
     "text/csv" ,
     "text/html" ,
     "text/xml" ,
     "text/plain" ,
     "image/png" ,
     "image/bmp"
};

cont_typ content_type = ct_max;

const char *get_content_type()
{
    //return content_types[content_type];
    return cont_types[content_type].name;
}
bool is_content_binary()
{
    return cont_types[content_type].is_binary;
}

void show_extensions()
{
    int i;
    char *cp = GetNxtBuf();
    char *tmp;
    for (i = ct_appjson; i < ct_max; i++) {
        strcpy(cp,cont_types[i].name);
        tmp = strchr(cp,'/');
        if (tmp) {
            if (strcmp(&tmp[1],"plain"))
                SPRTF( ".%s ",&tmp[1]);
            else
                SPRTF( ".txt ");
        } else {
            SPRTF("%s ",cp);
        }
    }
}

bool set_extension_type(char *file)
{
    std::string f = get_file_name(file);
    std::string::size_type pos = f.rfind('.');
    if (pos > 0) {
        int i;
        char *cp = GetNxtBuf();
        char *tmp;
        f = f.substr(pos+1);
        if (stricmp(f.c_str(),"txt") == 0) {
            content_type = ct_txtplain;
            return true;
        }
        for (i = ct_appjson; i < ct_max; i++) {
            //strcpy(cp,content_types[i]);
            strcpy(cp,cont_types[i].name);
            tmp = strchr(cp,'/');
            if (tmp) {
                if (stricmp(f.c_str(),&tmp[1]) == 0) {
                    content_type = (cont_typ)i;
                    return true;
                }
            }
        }
    }
    return false;
}

int Socketselect ( MSocket** reads, MSocket** writes, int timeout )
{
    fd_set r,w;
    int	retval, fd;

    FD_ZERO (&r);
    FD_ZERO (&w);

    int i, k ;
    int num = 0 ;

    if ( reads ) {
        for ( i = 0; reads[i]; i++ ) {
            fd = reads[i]->getHandle();
            if (fd != -1) {
                FD_SET (fd, &r);
                num++;
            }
        }
    }

    if ( writes ) {
        for ( i = 0; writes[i]; i++ ) {
            fd = writes[i]->getHandle();
            if (fd != -1) {
                FD_SET (fd, &w);
                num++;
            }
        }
    }

    if (!num)
        return num ;

    /* Set up the timeout */
    struct timeval tv ;
    tv.tv_sec  = timeout / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;

    retval = select (FD_SETSIZE, &r, &w, 0, &tv);
    // remove sockets that had no activity
    num = 0 ;

    if ( reads ) {
        for ( k = i = 0; reads[i]; i++ )
        {
            fd = reads[i]->getHandle();
            if (fd != -1) {
                if ( FD_ISSET (fd, &r) ) {
                    reads[k++] = reads[i];
                    num++;
                }
            }
        }
        reads[k] = NULL ;
    }

    if ( writes ) {
        for ( k = i = 0; writes[i]; i++ ) {
            fd = writes[i]->getHandle();
            if (fd != -1) {
                if ( FD_ISSET (fd, &w) ) {
                    writes[k++] = writes[i];
                    num++;
                }
            }
            writes[k] = NULL ;
        }
    }

    if (retval == 0) // timeout
        return (-2);
    if (retval == -1)// error
        return (-1);

    return num;
}

bool isNonBlockingError ()
{
#if defined(WIN32)
    int wsa_errno = WSAGetLastError();
    if ( wsa_errno != 0 ) {
        WSASetLastError(0);
        switch (wsa_errno) {
        case WSAEWOULDBLOCK: // always == NET_EAGAIN?
        case WSAEALREADY:
        case WSAEINPROGRESS:
            return true;
        }
    }
#else
    switch (errno) {
    case EWOULDBLOCK: // always == NET_EAGAIN?
    case EALREADY:
    case EINPROGRESS:
        return true;
    }
#endif
    return false;
}



// eof
