/*
    HTTP - A Suite of Portable Http Libraries and Apps

    Copyright (C) 2014  Geoff R. McLane - reoprts _at_ geoffair _dot_ info
 
    These libraries and apps are free software; you can redistribute them and/or
    modify them under the terms of the GNU General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.
 
    These libraries and apps are distributed in the hope that they will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.
 
    You should have received a copy of the GNU General Public
    License along with these libraries and apps; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA
 
    For further information visit http://geoffair.org

*/
#ifndef _HTTP_UTILS_HXX_
#define _HTTP_UTILS_HXX_

enum cont_typ {
    ct_appjson,
    ct_txtcsv,
    ct_txthtml,
    ct_txtxml,
    ct_txtplain,
    ct_imgpng,
    ct_imgbmp,
    ct_max
};

extern cont_typ content_type;

extern bool set_extension_type(char *file);
extern void show_extensions();
extern const char *get_content_type();
extern bool is_content_binary();

/////////////////////////////////////////////////
class MSocket
{
    int handle;
public:
    MSocket () { handle = -1; }
    ~MSocket () { }
    int getHandle () { return handle; }
    void setHandle (int hand) { handle = hand; }
};

extern int Socketselect ( MSocket** reads, MSocket** writes, int timeout );
extern bool isNonBlockingError ();

/////////////////////////////////////////////////

#endif // #ifndef _HTTP_UTILS_HXX_
// eof

