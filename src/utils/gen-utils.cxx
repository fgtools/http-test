// gen-utils.cxx

#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <stdio.h>
#ifdef _MSC_VER
#include <WinSock2.h>
#else
#include <string.h> // strlen(), ...
#endif
#include <time.h>
#include "sprtf.hxx"
#include "gen-utils.hxx"

static const char *module = "gen-utils";

//////////////////////////////////////////////////////
#ifdef _MSC_VER
// get a message from the system for this error value
char *get_errmsg_text( int err )
{
    LPSTR ptr = 0;
    DWORD fm = FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&ptr, 0, NULL );
    if (ptr) {
        size_t len = strlen(ptr);
        while(len--) {
            if (ptr[len] > ' ') break;
            ptr[len] = 0;
        }
        if (len) return ptr;
        LocalFree(ptr);
    }
    return 0;
}

void win_wsa_perror( char *msg )
{
    int err = WSAGetLastError();
    LPSTR ptr = get_errmsg_text(err);
    if (ptr) {
        SPRTF("%s = %s (%d)\n", msg, ptr, err);
        LocalFree(ptr);
    } else {
        SPRTF("%s %d\n", msg, err);
    }
}

#endif // _MSC_VER
//////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#define M_IS_DIR _S_IFDIR
#else
#define M_IS_DIR S_IFDIR
#endif

static struct stat stat_buf;
// return: 0 = not file or directory, 1 = file, 2 = directory, 
int is_file_or_directory( char *file )
{
    int iret = 0;
    if (stat(file,&stat_buf) == 0) {
        if (stat_buf.st_mode &  M_IS_DIR) 
            iret = 2;
        else
            iret = 1;
    }
    return iret;
}

size_t get_last_file_size() { return stat_buf.st_size; }

//////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////
static const char *ts_form = "%04d-%02d-%02d %02d:%02d:%02d";
// Creates the UTC time string
char *Get_UTC_Time_Stg(time_t Timestamp)
{
    static char s_utc_time[256];
    //char *ps = GetNxtBuf();
    char *ps = s_utc_time;
    tm  *ptm;
    ptm = gmtime (& Timestamp);
    sprintf (
        ps,
        ts_form,
        ptm->tm_year+1900,
        ptm->tm_mon+1,
        ptm->tm_mday,
        ptm->tm_hour,
        ptm->tm_min,
        ptm->tm_sec );
    return ps;
}

char *Get_Current_UTC_Time_Stg()
{
    time_t Timestamp = time(0);
    return Get_UTC_Time_Stg(Timestamp);
}
/////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////
char *get_file_name(char *file)
{
	char *name = file;
	size_t len = strlen(file);
	size_t ii;
	int c;
	for (ii = 0; ii < len; ii++) {
		c = file[ii];
		if ((c == '\\')||(c == '/')) {
			if ((ii + 1) < len)
				name = &file[ii+1];
		}
	}
	return name;
}

////////////////////////////////////////////////////////////////////
static void dummy()
{
	SPRTF("%s: dummy\n", module);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// added 20140918

///////////////////////////////////////////////////////////////////////////////
// FUNCTION   : InStr
// Return type: INT 
// Arguments  : LPTSTR lpb
//            : LPTSTR lps
// Description: Return the position of the FIRST instance of the string in lps
//              Emulates the Visual Basic function.
///////////////////////////////////////////////////////////////////////////////

int InStr( char *lpb, char *lps )
{
   int   iRet = 0;
   int   i, j, k, l, m;
   char  c;
   i = (int)strlen(lpb);
   j = (int)strlen(lps);
   if( i && j && ( i >= j ) )
   {
      c = *lps;   // get the first we are looking for
      l = i - ( j - 1 );   // get the maximum length to search
      for( k = 0; k < l; k++ )
      {
         if( lpb[k] == c )
         {
            // found the FIRST char so check until end of compare string
            for( m = 1; m < j; m++ )
            {
               if( lpb[k+m] != lps[m] )   // on first NOT equal
                  break;   // out of here
            }
            if( m == j )   // if we reached the end of the search string
            {
               iRet = k + 1;  // return NUMERIC position (that is LOGICAL + 1)
               break;   // and out of the outer search loop
            }
         }
      }  // for the search length
   }
   return iRet;
}

int InStri( char *lpb, char *lps )
{
   int   iRet = InStr(lpb,lps);
   if (iRet)
       return iRet;
   int   i, j, k, l, m;
   char  c, d;
   i = (int)strlen(lpb);
   j = (int)strlen(lps);
   if( i && j && ( i >= j ) )
   {
      c = toupper(*lps);   // get the first we are looking for
      l = i - ( j - 1 );   // get the maximum length to search
      for( k = 0; k < l; k++ )
      {
         d = toupper(lpb[k]);
         if( d == c )
         {
            // found the FIRST char so check until end of compare string
            for( m = 1; m < j; m++ )
            {
               if( toupper(lpb[k+m]) != toupper(lps[m]) )   // on first NOT equal
                  break;   // out of here
            }
            if( m == j )   // if we reached the end of the search string
            {
               iRet = k + 1;  // return NUMERIC position (that is LOGICAL + 1)
               break;   // and out of the outer search loop
            }
         }
      }  // for the search length
   }
   return iRet;
}

void ensure_win_seps( std::string &path )
{
    size_t pos = path.find('/');
    while (pos != (size_t)-1) {
        path.replace(pos,1,"\\");
        pos = path.find('/');
    }
}

void ensure_unix_seps( std::string &path )
{
    size_t pos = path.find('\\');
    while (pos != (size_t)-1) {
        path.replace(pos,1,"/");
        pos = path.find('\\');
    }
}

// eof


