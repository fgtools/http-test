@setlocal
@set TMPINST=0
@set TMPLOG=bldlog-1.txt
@set TMPPRJ=http
@set TMPOPTS=
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@if "%TMPOPTS%x" == "x" (
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:/MDOS
)

@call chkmsvc %TMPPRJ%

@echo Begin %DATE% %TIME% > %TMPLOG%

@echo Doing: 'cmake .. %TMPOPTS%'
@echo Doing: 'cmake .. %TMPOPTS%' >> %TMPLOG%
@cmake .. %TMPOPTS% >> %TMPLOG% 2>&1 >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR1

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing 'cmake --build . --config Release'
@echo Doing 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@echo.
@echo Appears successful build of %TMPPRJ%... See %TMPLOG% for details...

@if "%TMPINST%x" == "1x" goto DOINSTALL
@echo.
@echo Install is OFF at this time...
@echo.
@goto END

:DOINSTALL
@echo.
@echo Continue with install of the release configuration?
@echo Only Ctrl+C aborts... all other keys continue
@echo.

@pause

@REM cmake -DBUILD_TYPE=Release -P cmake_install.cmake
@echo Doing 'cmake --build . --config Release --target INSTALL'
@echo Doing 'cmake --build . --config Release --target INSTALL'  >> %TMPLOG%
@cmake --build . --config Release --target INSTALL  >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR4

@echo Done install...

@echo Test of file-server...
file-server -?
@if ERRORLEVEL 1 goto ERR5

@echo All done...

@goto END

:ERR1
@echo cmake config or generation error
@goto ISERR

:ERR2
@echo debug build error
@goto ISERR

:ERR3
@echo release build error
@goto ISERR

:ERR4
@echo install error
@goto ISERR

:ERR5
@echo run of file-server failed
@goto ISERR

:ERR6
@echo run of wmm_magvar failed
@goto ISERR

:ERR7
@echo run of testmagvar failed
@goto ISERR

:ISERR
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
