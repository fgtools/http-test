# HTTP Experiments

#### mongoose library

Lovely library for taking the strain out of build a http server - https://code.google.com/p/mongoose/ - as it says on its web site `Mongoose is the most easy to use web server on the planet.`.

#### file-server app

```
file-server - version 0.0.2

Usage: file-server [options] in_file

Options:
 --help   (-h or -?) = This help and exit(0)
 --port nnnn    (-p) = Set port (def=5555)
 --addr <ip>    (-a) = Set the IP address. (def=127.0.0.1)
 --sleep <ms>   (-s) = Set milliseconds sleep after sned. 0 for none. (def=100)
 --timeout <ms> (-t) = Set milliseconds select timeout. (def=500)
 --log <file>   (-l) = Set log file.
    (def=C:\Users\user\AppData\Local\templog.txt)

Listen on address:port, and on connection send in_file using
the file extension to set the Content-type.
Allowed extensions .json .csv .html .xml .txt .png .bmp

Use an ESC keyin to exit the continuous server loop.
```

#### http-server app

```
http-server - version 1.0.0

Usage: http-server [options]

Options:
 --help   (-h or -?) = This help and exit(2)
 --port <num>   (-p) = Set port (def=5555)
 --log <file>   (-l) = Set log file. (def=temphttp.txt, in CWD)
 --sleep <ms>   (-s) = Set milliseconds sleep in loop. 0 for none. (def=100)
 --timeout <ms> (-t) = Set milliseconds timeout for select(). (def=500)
 --verb[num]    (-v) = Bump or set verbosity. (def=0)
 --dir <dir>    (-d) = Set directory for file searching.
   Default direcotry is 'C:\OSGeo4W\apache\htdocs\map-test2'

Will establish a HTTP server on the port, and respond to GET with '/file-name'

All others will return 400 - command error, or 404 - file not found

All output will be written to stdout, and the log file.
Will exit on ESC keying, if in foreground
```

#### ipv6-server app

```
Simple socket sample server program.

release\ipv6-server [-f family] [-t transport] [-p port] [-a address]

  family        One of PF_INET, PF_INET6 or PF_UNSPEC.  (default PF_UNSPEC)
  transport     Either TCP or UDP.  (default: TCP)
  port          Port on which to bind.  (default 5001)
  address       IP address on which to bind.  (default: unspecified address)
```

This is a windows app, but there is also a roughtly equivalent ipv6-serveru app, but it is less developed.

#### win-protos app

This is ONLY for windows!

```
win-protos: usage: [options]
Options:
 --help  (-h or -?) = This help and exit(2)

 Sole purpose is to enumerate protocols installed in this
 Windows WIN7-PC computer.
```

Enjoy!

Geoff  
20150613 - 20150612

; eof
